@file:Suppress("UNUSED_EXPRESSION")

package cat.itb.m05.uf2

import java.util.*

/**
 * @input m=3, n=7
 * @return los numeros que hay entre m y n
 * @output 3 4 5 6 7*/
fun needFunctionName(m: Int, n: Int): List<Int> {
    val numero = mutableListOf<Int>()
    for (i in m..n) {
        numero.add(i)
    }
    return numero
}
/**@return el numero mas pequeño de la lista de enteros*/
fun minValue(a: List<Int>): Int {
    var b = 0
    for (i in a.indices) {
        if (i < b) {
            b = i
        } else if (i == b) {
            b = i
        }
    }
    return b
}

/** @when expresa el siguiente comportamiento
  0: No presentat
  1-4: Insuficient
  5-6: Suficient
  7-8: Notable
  9: Excel·lent
  10: Excel·lent + MH
 *@return no valid si no es un caracter del when.
*/
fun select(i: Int): String {
    //TODO: Cahotic order. Make it easy, only one return.
    when (i) {
        in 1..4 -> "Insuficient"
        in 5..6 -> "Suficient"
        in 7..8 -> "Notable"
        in 9..10 -> "Excelent + MH"
        else -> "No Presentat"
    }
    return "No valid"
}

 /**@return toda la palabra*/
fun charPositionsInString(abc: String, b: Char): List<Int> {
    val s = mutableListOf<Int>()
    for (i in 0 until abc.lastIndex) {
        if (abc[i] == b) {
            s.add(i)
        }
    }
    return  s
}


/**
 * @if si abc es igual a b
 * abc="marieta" b='a' -> 1
 * abc="marieta" b='b' -> -1
*/
fun firstOccurrencePosition(abc: String, b: Char): Int {
    for (i in 0 until abc.length) {
        if (abc[i] == b) {
            return i
        }
    }
    return -1
}

/**
 * @return la nota redondeada. 8.7 -> 9, 7.5 -> 8
 * @if Si la nota no arriba a un 5, no obtindrà el 5. És a dir: 4.9 -> 4
 */
fun note(grade: Double): Int {
    return grade.toInt()
}